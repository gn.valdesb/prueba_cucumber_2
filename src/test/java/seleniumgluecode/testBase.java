package seleniumgluecode;

import org.openqa.selenium.WebDriver;
import pom.comicsPage;
import pom.homePage;

public class testBase {

    protected WebDriver driver = Hooks.getDriver();
    protected homePage homePage = new homePage(driver);
    protected comicsPage comicsPage = new comicsPage(driver);

}
