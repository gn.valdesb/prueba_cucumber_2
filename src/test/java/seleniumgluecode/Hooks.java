package seleniumgluecode;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.WebDriver;
import runner.browserManager.DriverManager;
import runner.browserManager.DriverManagerFactory;
import runner.browserManager.DriverType;

public class Hooks {

    private static WebDriver driver;
    private static int numberOfCase = 0;
    private DriverManager driverManager;

    @Before
    public void setUP(){
        numberOfCase++;
        System.out.println("Se esta ejecutando el escenario nro: "+numberOfCase);
        //Se configura el tipo de navegador
        driverManager = DriverManagerFactory.getManager(DriverType.CHROME);
        //Inicializa la variable driver con el tipo de navegador
        driver = driverManager.getDriver();
        driver.get("https://imalittletester.com/");
        driver.manage().window().maximize();
    }

    @After
    public void tearDown(){
        System.out.println("El escenario nro: "+numberOfCase+" se ejecuto correctamente");
        driverManager.quitDriver();
    }

    public static WebDriver getDriver(){
        return driver;
    }

}
