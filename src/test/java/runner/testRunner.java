package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)

@CucumberOptions(
        //Ruta donde buscara los features
        features = "src/test/java/features",
        //Ruta donde buscara la implamentacion de los pasos
        glue = {"seleniumgluecode"}
)
public class testRunner {


}
