package pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class comicsPage extends basePage{
    private By pageTitleLocate = By.className("page-title");
    private String titlePage = "Category: comics";

    public comicsPage(WebDriver driver){
        super(driver);
    }

    public boolean isTitleComicsDisplayed() throws Exception {
        return this.isDisplayed(pageTitleLocate) && this.getText(pageTitleLocate).equals(titlePage);
    }
}
