package seleniumgluecode;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Test extends testBase{

    @Given("^El susuario se encuentr en la pangina Home de imalittletester$")
    public void el_susuario_se_encuentr_en_la_pangina_Home_de_imalittletester() throws Throwable {
        Assert.assertTrue(homePage.homePageIsDisplayed());
    }

    @When("^Hace click en el boton The Little Tester comics$")
    public void hace_click_en_el_boton_The_Little_Tester_comics() throws Throwable {
        homePage.clickOnTitleComics();
    }

    @Then("^Se debe re dirigir a la pantalla Comics$")
    public void se_debe_re_dirigir_a_la_pantalla_Comics() throws Throwable {
        Assert.assertTrue("No se redirecciono correctamente a la pagina de comics", comicsPage.isTitleComicsDisplayed());
    }
}
