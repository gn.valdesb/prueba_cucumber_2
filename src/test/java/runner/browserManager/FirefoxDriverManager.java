package runner.browserManager;

import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxDriverManager extends DriverManager {

    @Override
    public void createDriver(){
        System.setProperty("webdriver.chrome.driver", "./src/test/resources/geckodriver-v0.29.1-win32/geckodriver.exe");
        driver = new FirefoxDriver();
    }
}
